-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.9-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.4.0.5168
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for simantra
DROP DATABASE IF EXISTS `simantra`;
CREATE DATABASE IF NOT EXISTS `simantra` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `simantra`;

-- Dumping structure for table simantra.tbl_customer
DROP TABLE IF EXISTS `tbl_customer`;
CREATE TABLE IF NOT EXISTS `tbl_customer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `no_registrasi` varchar(50) NOT NULL DEFAULT '0',
  `nama` varchar(50) NOT NULL DEFAULT '0',
  `alamat` varchar(100) NOT NULL DEFAULT '0',
  `no_handphone` int(13) unsigned NOT NULL DEFAULT '0',
  `tempat_lahir` varchar(50) NOT NULL DEFAULT '0',
  `tanggal_lahir` date NOT NULL,
  `kelamin` enum('Laki - Laki','Perempuan') NOT NULL DEFAULT 'Laki - Laki',
  `pekerjaan` varchar(50) NOT NULL DEFAULT '0',
  `produk` varchar(50) NOT NULL DEFAULT '0',
  `varian_id` int(11) unsigned NOT NULL,
  `perusahaan_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `no_registrasi` (`no_registrasi`),
  KEY `customer_perusahaan_id` (`perusahaan_id`),
  KEY `customer_varian_id` (`varian_id`),
  CONSTRAINT `customer_perusahaan_id` FOREIGN KEY (`perusahaan_id`) REFERENCES `tbl_perusahaan` (`id`) ON DELETE CASCADE,
  CONSTRAINT `customer_varian_id` FOREIGN KEY (`varian_id`) REFERENCES `tbl_varian` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simantra.tbl_harga_varian
DROP TABLE IF EXISTS `tbl_harga_varian`;
CREATE TABLE IF NOT EXISTS `tbl_harga_varian` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `varian_id` int(11) unsigned NOT NULL,
  `kode_hrga` varchar(50) NOT NULL,
  `room_type` tinyint(1) unsigned NOT NULL,
  `harga` double(10,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_hrga` (`kode_hrga`),
  KEY `varian_harga_id` (`varian_id`),
  CONSTRAINT `varian_harga_id` FOREIGN KEY (`varian_id`) REFERENCES `tbl_varian` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simantra.tbl_paket
DROP TABLE IF EXISTS `tbl_paket`;
CREATE TABLE IF NOT EXISTS `tbl_paket` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `produk_id` int(11) unsigned NOT NULL DEFAULT '0',
  `kode_paket` varchar(50) NOT NULL DEFAULT '0',
  `tgl_berangkat` datetime NOT NULL,
  `tgl_pulang` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_paket` (`kode_paket`),
  KEY `produk_paket_id` (`produk_id`),
  CONSTRAINT `produk_paket_id` FOREIGN KEY (`produk_id`) REFERENCES `tbl_produk` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simantra.tbl_perusahaan
DROP TABLE IF EXISTS `tbl_perusahaan`;
CREATE TABLE IF NOT EXISTS `tbl_perusahaan` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode_perusahaan` varchar(50) NOT NULL DEFAULT '0',
  `nama` varchar(50) DEFAULT '0',
  `alamat` varchar(150) DEFAULT '0',
  `no_telp` int(13) unsigned DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_perusahaan` (`kode_perusahaan`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simantra.tbl_produk
DROP TABLE IF EXISTS `tbl_produk`;
CREATE TABLE IF NOT EXISTS `tbl_produk` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `kode_produk` varchar(20) NOT NULL DEFAULT '0',
  `nama` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_produk` (`kode_produk`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simantra.tbl_user
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE IF NOT EXISTS `tbl_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `perusahaan_id` int(11) unsigned NOT NULL DEFAULT '0',
  `kode_user` varchar(50) NOT NULL DEFAULT '0',
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  `nama` varchar(50) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '0',
  `tempat_lahir` varchar(50) NOT NULL DEFAULT '0',
  `tanggal_lahir` date NOT NULL,
  `role` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_user` (`kode_user`),
  UNIQUE KEY `email` (`email`),
  KEY `perusahaan_user_id` (`perusahaan_id`),
  CONSTRAINT `perusahaan_user_id` FOREIGN KEY (`perusahaan_id`) REFERENCES `tbl_perusahaan` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
-- Dumping structure for table simantra.tbl_varian
DROP TABLE IF EXISTS `tbl_varian`;
CREATE TABLE IF NOT EXISTS `tbl_varian` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `paket_id` int(11) unsigned NOT NULL,
  `kode_varian` varchar(50) NOT NULL,
  `quad` double(10,2) NOT NULL,
  `triple` double(10,2) NOT NULL,
  `double` double(10,2) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kode_varian` (`kode_varian`),
  KEY `paket_varian_id` (`paket_id`),
  CONSTRAINT `paket_varian_id` FOREIGN KEY (`paket_id`) REFERENCES `tbl_paket` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Data exporting was unselected.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
