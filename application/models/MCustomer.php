<?php
/**
 *
 */
class MCustomer extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_customer');
    $crud->set_subject('Customer');
    $crud->required_fields('kode_perusahaan');
    $crud->columns('no_registrasi','nama', 'alamat', 'no_handphone', 'tempat_lahir', 'tanggal_lahir', 'kelamin', 'pekerjaan', 'produk', 'varian_id', 'perusahaan_id');

    return $crud->render();
  }
}

?>
