<?php
/**
 *
 */
class MVarian extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_varian');
    $crud->set_subject('Varian');
    $crud->required_fields('kode_varian');
    $crud->columns('paket_id', 'kode_varian','quad', 'triple', 'double', 'lokasi');

    return $crud->render();
  }
}

?>
