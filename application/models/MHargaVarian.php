<?php
/**
 *
 */
class MHargaVarian extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_harga_varian');
    $crud->set_subject('Harga Varian');
    $crud->required_fields('kode_harga');
    $crud->columns('varian_id', 'kode_harga','room_type', 'harga');

    return $crud->render();
  }
}

?>
