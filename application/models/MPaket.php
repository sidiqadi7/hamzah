<?php
/**
 *
 */
class MPaket extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_paket');
    $crud->set_subject('Paket');
    $crud->required_fields('kode_paket');
    $crud->columns('kode_paket','tgl_berangkat', 'tgl_pulang', 'produk_id');

    return $crud->render();
  }
}

?>
