<?php
/**
 *
 */
class MUser extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_user');
    $crud->set_subject('User');
    $crud->required_fields('kode_user');
    $crud->columns('perusahaan_id','kode_user', 'username', 'passsword', 'nama', 'email', 'tempat_lahir', 'tanggal_lahir', 'role');

    return $crud->render();
  }
}

?>
