<?php
/**
 *
 */
class MProduct extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_produk');
    $crud->set_subject('Produk');
    $crud->required_fields('kode_produk');
    $crud->columns('nama','kode_produk');

    return $crud->render();
  }
}

?>
