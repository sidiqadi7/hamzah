<?php
/**
 *
 */
class MPerusahaan extends CI_Model
{

  function __construct()
  {
    parent::__construct();
  }

  public function show(){
    $crud = new grocery_CRUD();

    $crud->set_theme('datatables');
    $crud->set_table('tbl_perusahaan');
    $crud->set_subject('Perusahaan');
    $crud->required_fields('kode_perusahaan');
    $crud->columns('nama','kode_perusahaan', 'alamat', 'no_telp');

    return $crud->render();
  }
}

?>
