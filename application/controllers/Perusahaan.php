<?php
/**
 *
 */
class Perusahaan extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

		$this->load->database();

    $this->load->model('MPerusahaan');
  }

  public function index(){
    $output = $this->MPerusahaan->show();
    $this->load->view('perusahaan/index',(array)$output);
  }

}


 ?>
