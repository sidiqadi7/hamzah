<?php
/**
 *
 */
class Varian extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

		$this->load->database();

    $this->load->model('MVarian');
  }

  public function index(){
    $output = $this->MVarian->show();
    $this->load->view('varian/index',(array)$output);
  }

}


 ?>
