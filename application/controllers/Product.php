<?php
/**
 *
 */
class Product extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

		$this->load->database();

    $this->load->model('MProduct');
  }

  public function index(){
    $output = $this->MProduct->show();
    $this->load->view('product/index',(array)$output);
  }

}


 ?>
