<?php
/**
 *
 */
class Customer extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

		$this->load->database();

    $this->load->model('MCustomer');
  }

  public function index(){
    $output = $this->MCustomer->show();
    $this->load->view('customer/index',(array)$output);
  }

}


 ?>
