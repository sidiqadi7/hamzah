<?php
/**
 *
 */
class User extends CI_Controller
{

  function __construct()
  {
    parent::__construct();

		$this->load->database();

    $this->load->model('MUser');
  }

  public function index(){
    $output = $this->MUser->show();
    $this->load->view('user/index',(array)$output);
  }

}


 ?>
